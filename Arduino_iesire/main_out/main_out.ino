//-----------------------------------------------------------------------------------------
//Proiect: Management parcare hotel
//Partea de iesire
//-----------------------------------------------------------------------------------------
//adresa modulului Bluetooth corespunzator partii de iesire: 00:28:13:00:23:94
//-----------------------------------------------------------------------------------------
#include <Servo.h> //pertru servomotor
//pentru modulul RFID - RC522
#include <deprecated.h>
#include <MFRC522.h>
#include <MFRC522Debug.h>
#include <MFRC522Extended.h>
#include <MFRC522Hack.h>
#include <require_cpp11.h>
//-----------------------------------------------------------------------------------------
#include <Wire.h>
//implementare pentru i2c
//optionala
//pentru aceasta implementare partea de comanda va avea codul 1 , partea de intrare codul 2
//partea de iesire codul 3
int w_state_in = -1; //variabila folosita pentru comunicatia i2c
int w_state_out = -1; //variabila folosita pentru comunicatia i2c
//-----------------------------------------------------------------------------------------
constexpr uint8_t RST_PIN = 8;       // Configurable, see typical pin layout above
constexpr uint8_t SS_PIN = 10;       // Configurable, see typical pin layout above
MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance
//-----------------------------------------------------------------------------------------
int  ok = 0; //variabila folosita pentru card-ul RFID
//-----------------------------------------------------------------------------------------
Servo myservo; //definim servomotor-ul
//-----------------------------------------------------------------------------------------
void setup() {
  myservo.attach(9); //pin-ul atasat servomotorului
  //Serial.begin(9600);///for testing
  Serial.begin(38400);///for Bluetooth
  //---------------------------------------------------------------------------------------
  //pentru comunicatia i2c
  //partea de iesire are codul 3
  Wire.begin(3); 
  //cand se primesc date prin i2c atunci se apeleaza functia requestEvent()
  Wire.onRequest(requestEvent); //partea de iesire ofera date
  Wire.onReceive(receiveEvent); //partea de iesire primeste date
  //---------------------------------------------------------------------------------------
  SPI.begin();      // Init SPI bus
  mfrc522.PCD_Init();   // Init MFRC522
  mfrc522.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details
  //---------------------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------------------
void requestEvent() {
  //functia care se apeleaza in momentul cand slave-ul transmite date
  Wire.write(w_state_out);
}
void receiveEvent() {
  while (1 < Wire.available()) { // loop through all but the last
    w_state_in = Wire.read();//citim datele transmise
}
//-----------------------------------------------------------------------------------------
//functie ce controleaza ridicarea si coborarea barierei dupa un timp
//aici am folosit mai multe comenzi pentru servomotor deoarece bariera 
//nu se ridica brusc la 90* 
//bariera se ridica in trepte
//asta a fost ideea functiei
void servomotor_control()
  {
  myservo.write(15);
  delay(50);
  myservo.write(30);
  delay(50);
  myservo.write(45);
  delay(50);
  myservo.write(75);
  delay(50);
  myservo.write(90);
  delay(9600);
  myservo.write(0);
  }
//-----------------------------------------------------------------------------------------
void look_card()
  {
  // cauta carduri noi
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }

  // selecteaza un card
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }
  ok = 1; //daca s-au gasit card-uri noi
  }
//-----------------------------------------------------------------------------------------
void card_test()
{
  String content= "";
  byte letter;
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
    //Serial.print(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");//transmitem date pe seriala
    //Serial.print(mfrc522.uid.uidByte[i], HEX);//transmitem date pe seriala
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  content.toUpperCase();
  if (content.substring(1) == "9E C6 D7 20" )
  {
    if(w_state_in == '0') //daca clientul s-a decazat
      {
        w_state_out = 0;
        //s-a decazat camera 1
      }
      else{
        if ( ok == 1 )
        {
          servomotor_control();//functie ce controleaza bariera
          ok = 0;
          delay(3000);
        }
      
      else   {
          //Serial.println(" Access denied");//testing
          delay(3000);
        }
      }
  }
  if( content.substring(1) == "3F F8 8C 29" ) 
  {
    if(w_state_in == '0') //daca clientul s-a decazat
      {
        w_state_out = 1;
        //s-a decazat camera 2
      }
      else{
        if ( ok == 1 )
        {
          servomotor_control();//functie ce controleaza bariera
          ok = 0;
          delay(3000);
        }
      
      else   {
          //Serial.println(" Access denied");//testing
          delay(3000);
        }
      }
  }
  if (content.substring(1) == "6B BC 4A 73") 
  {
    if(w_state_in == '0') //daca clientul s-a decazat
      {
        w_state_out = 2;
        //s-a decazat camera 3
      }
      else{
        if ( ok == 1 )
        {
          servomotor_control();//functie ce controleaza bariera
          ok = 0;
          delay(3000);
        }
      
      else   {
          //Serial.println(" Access denied");//testing
          delay(3000);
        }
      }
  }
  //-----------------------------------------------------------------------------------------
  Wire.beginTransmission(1);
  Wire.write(w_state_out); //transmitem datele la comanda
  //in caz ca nu functioneaza functia requestEvent()
  Wire.endTransmission();
  //-----------------------------------------------------------------------------------------
  //verifica daca ID-ul tag-ului RFID se afla in lista
  //schimba cu ID-urile cardurilor pe care vrei sa le dai acces
  //totodata aceste ID-uri pot corespunde anumitor camere pentru a stii daca un anume client 
  //a intrat/parasit hotelul
  //aici ar trebuii verificata fiecare cartela in parte si transmis la comanda pentru a elibera camera 
  //asta daca clientul s-a decazat
  //aici ar trebuii primit un cod pe seriala/i2c

} 
//-----------------------------------------------------------------------------------------
void loop() {
  //RFID
  MFRC522::MIFARE_Key key;
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  MFRC522::StatusCode status;
  //---------------------------------------------------------------------------------------
  look_card(); // cauta card-uri noi sau deja existente
  //---------------------------------------------------------------------------------------
  //detalii despre cartela RFID
  //mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid)); 
  //testing
  //---------------------------------------------------------------------------------------
  card_test();
  //---------------------------------------------------------------------------------------
  delay(250);//asteptam 250ms
  //---------------------------------------------------------------------------------------
  // Halt PICC
  mfrc522.PICC_HaltA();
  // Stop encryption on PCD
  mfrc522.PCD_StopCrypto1();
}
