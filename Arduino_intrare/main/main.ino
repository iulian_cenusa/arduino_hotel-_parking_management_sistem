//---------------------------------------------------------------------------------------
//Proiect: Management parcare hotel
//Partea de intrare
//---------------------------------------------------------------------------------------
//adresa modulului Bluetooth corespunzator partii de intrare: 00:28:13:00:20:7E
//---------------------------------------------------------------------------------------
#include <Wire.h>
//implementare pentru i2c
//optionala
//pentru aceasta implementare partea de comanda va avea codul 1 , partea de intrare codul 2
//partea de iesire codul 3
int w_state; //variabila folosita pentru transmisia i2c
//-----------------------------------------------------------------------------------------
#include <Servo.h> //pertru servomotor
//pentru modulul RFID - RC522
#include <deprecated.h>
#include <MFRC522.h>
#include <MFRC522Debug.h>
#include <MFRC522Extended.h>
#include <MFRC522Hack.h>
#include <require_cpp11.h>
//---------------------------------------------------------------------------------------
constexpr uint8_t RST_PIN = 8;       // Configurable, see typical pin layout above
constexpr uint8_t SS_PIN = 10;       // Configurable, see typical pin layout above
MFRC522 mfrc522(SS_PIN, RST_PIN);  // Create MFRC522 instance
//---------------------------------------------------------------------------------------
#define BTN 2 //definim pin-ul pentru buton
//#define LED_BTN 3 //definim pin-ul petnru led-ul de la intrarea in parcare
//#define LED_RFID 4 //definim pin-ul petnru led-ul de la intrarea in parcare
int  ok = 0; //variabila folosita pentru card-ul RFID
//---------------------------------------------------------------------------------------
Servo myservo; //definim servomotor-ul
int stare = 0; //variabila in care stocam starea curenta a butonului
//---------------------------------------------------------------------------------------
void setup() {
  pinMode( BTN , INPUT ); //definim butonul ca input
  //pinMode( LED_BTN , OUTPUT ); //definim butonul ca input
  //pinMode( LED_RFID , OUTPUT ); //definim butonul ca input
  myservo.attach(9); //pin-ul atasat servomotorului
  //Serial.begin(9600);///for testing
  Serial.begin(38400);///for Bluetooth
  //---------------------------------------------------------------------------------------
  //pentru comunicatia i2c
  //partea de intrare are codul 2
  Wire.begin(2); 
  //cand se cer date prin i2c atunci se apeleaza functia requestEvent()
  Wire.onRequest(requestEvent);
  //---------------------------------------------------------------------------------------
  SPI.begin();      // Init SPI bus
  mfrc522.PCD_Init();   // Init MFRC522
  mfrc522.PCD_DumpVersionToSerial();  // Show details of PCD - MFRC522 Card Reader details
  //---------------------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------------------
void requestEvent() {
  //functia care se apeleaza in momentul cand slave-ul transmite date
  Wire.write(w_state);//transmitem starea variabilei alocata pentru i2c
}
//-----------------------------------------------------------------------------------------
//functie ce controleaza ridicarea si coborarea barierei dupa un timp
//aici am folosit mai multe comenzi pentru servomotor deoarece bariera 
//nu se ridica brusc la 90* 
//bariera se ridica in trepte
//asta a fost ideea functiei
void servomotor_control()
  {
  myservo.write(15);
  delay(50);
  myservo.write(30);
  delay(50);
  myservo.write(45);
  delay(50);
  myservo.write(75);
  delay(50);
  myservo.write(90);
  delay(9600);
  myservo.write(0);
  }
//-----------------------------------------------------------------------------------------
void look_card()
  {
  // Cautam carduri noi
  if ( ! mfrc522.PICC_IsNewCardPresent()) {
    return;
  }else{
    //--------------------------------------------------------------------------------------
  }

  // Selectam unul dintre carduri
  if ( ! mfrc522.PICC_ReadCardSerial()) {
    return;
  }else{
    //--------------------------------------------------------------------------------------
  }
  ok = 1; //daca s-au gasit card-uri noi
  //---------------------------------------------------------------------------------------
  }
//-----------------------------------------------------------------------------------------
void card_test()
  {
  String content= ""; //variabila in care stocam informatiile despre card
  byte letter;
  //---------------------------------------------------------------------------------------
  for (byte i = 0; i < mfrc522.uid.size; i++) 
  {
    Serial.write(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " ");//transmitem date pe seriala
    //Serial.write(mfrc522.uid.uidByte[i], HEX);//transmitem date pe seriala
    content.concat(String(mfrc522.uid.uidByte[i] < 0x10 ? " 0" : " "));
    content.concat(String(mfrc522.uid.uidByte[i], HEX));
  }
  //---------------------------------------------------------------------------------------
  //Serial.println();
  //Serial.print("Message : ");
  content.toUpperCase(); //informatiile din sir vor trece in uppercase
  if (content.substring(1) == "9E C6 D7 20" or content.substring(1) == "3F F8 8C 29" or content.substring(1) == "6B BC 4A 73") 
  //verifica daca ID-ul tag-ului RFID se afla in lista
  //schimba cu ID-urile cardurilor pe care vrei sa le dai acces
  //totodata aceste ID-uri pot corespunde anumitor camere pentru a stii daca un anume client 
  //a intrat/parasit hotelul
    if ( ok == 1 ) 
    {
      //Serial.println("Acces autorizat");
      //Serial.println();
      //------------------------
      w_state = 1; //cod pentru deschierea barierei cu card RFID
      //------------------------
      //digitalWrite(LED_RFID,HIGH); //aprindem un LED
      //ca sa semnalam ca s-a activat cititorul RFID
      //acest LED este obtional
      servomotor_control(); //functia de control a barierei
      ok = 0; //
      delay(3000); //asteapta 3 secunde
    }
 else   {
    //Serial.println("Acces neautorizat");
    //digitalWrite(LED_RFID,LOW);// LED-ul ce semnaleaza activarea
    //cititorului RFID este stins sau ramane stins
    //acest LED este obtional
    delay(3000);//asteapta 3 secunde
  }
  } 
//-----------------------------------------------------------------------------------------
int state;//variabila folosita pentru comunicatia seriala
//-----------------------------------------------------------------------------------------
void loop() {
  // verifica daca avem date pe seriala
  if (Serial.available() > 0) { 
    state = Serial.read(); // citeste datele de pe seriala
  }
  MFRC522::MIFARE_Key key;//RFID
  for (byte i = 0; i < 6; i++) key.keyByte[i] = 0xFF;
  MFRC522::StatusCode status;
  //---------------------------------------------------------------------------------------
  look_card(); // cauta card-uri noi sau deja existente
  //---------------------------------------------------------------------------------------
  //mfrc522.PICC_DumpDetailsToSerial(&(mfrc522.uid)); 
  //afisam detalii despre card
  //---------------------------------------------------------------------------------------
  card_test();
  //functie in care avem un sir de caractere ce reprezinta
  //ID-ul cardului folosit
  //---------------------------------------------------------------------------------------
  stare = digitalRead(BTN);//daca apasam butonul
  //folosim buton pentru clientii noi care nu detin card RFID pentru acces in parcare
  //clientii cazati vor primii pe parcursul sederii la hotel un card RFID corespunzator
  //camerei in care vor fi cazati
  if( stare == HIGH )
  {
    //Serial.println( "Buton apasat" ); //testing
    //digitalWrite(LED_BTN,HIGH); //obtional LED
    //se aprinde LED-ul ce smenaleaza daca butonul a fost apasat
    //aceasta functie poate fi pur de testare
    //ulterior se poate renunta la acel LED
    servomotor_control();//controlam servomotorul
    //functie ce controleaza ridicarea si coborarea barierei dupa un timp
    Serial.write("nou"); //daca avem client nou ( buton apasat ) atunci se va trimite
    //o cerere catre partea de comanda, pentru a elibera un card nou sau 
    //sa trimita un mesaj ca toate camerele sunt ocupate
    //pentru acest lucru se poate utiliza un LED
    //se poate folosii unul din LED-urile definite pentru buton sau modul RFID
    //deoarece acestea nu sunt neaparat necesare
    //------------------------------------------
    //partea de i2c
    w_state = 0; // cod pentru buton apasat
    //------------------------------------------
  }
  else
    {
    //digitalWrite(LED_BTN,LOW); //obtional LED
    //LED-ul ce semnaleaza daca butonul a fost apasat este stins
    myservo.write(0); //reset servomotor
    //bariera ramane blocata
    }
   delay(250);//asteptam 250ms
   //---------------------------------------------------------------------------------------
   // Halt PICC
   mfrc522.PICC_HaltA();
   // Stop encryption on PCD
   mfrc522.PCD_StopCrypto1();
}
//------------------------------------------------------------------------------------------
