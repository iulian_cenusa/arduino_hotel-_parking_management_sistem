# Hotel Management System
#Autor : Cenusa Iulian-Dimitrie

#Proiectul are trei parti :
 -sistem de intrare
 -sistem de iesire
 -sitem de comanda
 
 #Sistemul de intrare
 
 -Aceasta v-a consta din Arduino UNO R3, un buton, un cititor RFID RC522, un servomotor si un modul Bluetooth HC-05.
 -Butonul are rol de input.
 -Cititorul RFID are rol de input.
 -Servomotorul are rol de output, simuland bariera care trebuie sa se deschida.
 -Arduino UNO are rol de procesare a informatiei de la input si comanda servomotorului.
 -Modulul bluetooth are rol de realizare a comunicatiei dintre sistemul de intrare si sistemul de comanda.
 
 #Sistemul de iesire
 -Aceasta constra din Arduino UNO, Cititor RFID RC522, servomotor si modul Bluetooth HC-05.
 -Cititorul RFID are rol de input.
 -Servomotorul are rol de output, simuland bariera care trebuie sa se deschida.
 -Arduino UNO are rol de procesare a informatiei de la cititorul RFID si comanda servomotorului.
 -Modulul bluetooth are rol de realizare a comunicatiei dintre sistemul de iesire si sistemul de comanda.
 
  #Sistemul de comanda
  -Aceasta consta din  Arduino UNO si modul Bluetooth HC-05.
  -Modulul bluetooth are rol de a comunica cu celelalte doua module bluetooth din sistemele de intrare si cel de iesire si de a realiza schimb de informatii cu acestea.
  -Arduino UNO in acest caz v-a folosii memoria EEPROM pentru stocare de date.
  