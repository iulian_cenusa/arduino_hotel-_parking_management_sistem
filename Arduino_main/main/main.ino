//-----------------------------------------------------------------------------------------
//Proiect: Management parcare hotel
//Partea de comanda
//-----------------------------------------------------------------------------------------
#include <EEPROM.h> //pentru scriere/citire date EEPROM
#define CAM1 0
#define CAM2 1
#define CAM3 2
#define CAM_GOL -1
//-----------------------------------------------------------------------------------------
#include <Wire.h>
//implementare pentru i2c
//optionala
//pentru aceasta implementare partea de comanda va avea codul 1 , partea de intrare codul 2
//partea de iesire codul 3
//-----------------------------------------------------------------------------------------
int BTN = 7; //pin buton
int stare_buton; //variabila pentru starea butonului
//-----------------------------------------------------------------------------------------
//o functie pe care aas vrea sa o implementez este generarea de facturi
//pe seriala , nu fizic
//asta as vrea sa o fac in partea de comanda
//-----------------------------------------------------------------------------------------
#include <SoftwareSerial.h>
//pentru comunciatia bluetooth
SoftwareSerial mySerial(10, 11); // RX, TX
//-----------------------------------------------------------------------------------------
//adresa modulului Bluetooth corespunzator partii de comanda: 00:21:13:00:06:F3
int addr = 0; //pentru adresele EEPROM
//-----------------------------------------------------------------------------------------
void setup() {
  mySerial.begin(38400);///for Bluetooth
  Serial.begin(9400);///for Bluetooth
  //---------------------------------------------------------------------------------------
  pinMode(BTN,INPUT);
  //---------------------------------------------------------------------------------------
  //pentru comunicatia i2c
  //master-ul are codul 1
  Wire.begin(1); 
  //---------------------------------------------------------------------------------------
}
//-----------------------------------------------------------------------------------------
//void requestEvent() {
//  //functia care se apeleaza in momentul cand slave-ul primeste date
//}
//-----------------------------------------------------------------------------------------
int state;//variabila folosita pentru seriala
int w_state; //variabila folosita la comunicatia i2c
//-----------------------------------------------------------------------------------------
void EEPROM_increment(){
  //daca s-a ajuns la ultima adresa EEPROM revenim la inceput
  addr = addr + 1;
  if (addr == EEPROM.length()) {
    addr = 0;
  }
}
//-----------------------------------------------------------------------------------------
int cnt = 0; //variabila folosita pentru numarul facturii
//int client = 0; //variabila ce contorizeaza numarul de clienti
//posibil sa nu fie necesara
//poate fi eliminata intr-o versiune ulterioara
void generare_factura(int x)
{
  //functia care generaza factura
  //o generaza in sensul ca o scrie pe seriala
  Serial.println("Factura ");
  Serial.print(cnt);
  //Serial.println("Emisa pentru clientul ");
  //Serial.print(client);
  if( x == 0 ) //addr == 0
  {
    Serial.println("\nCamera 1 ");
    Serial.println("Cartela cu ID-ul 9E C6 D7 20 \n");
  }
  if( x == 1 ) //addr == 1
  {
    Serial.println("\nCamera 2 ");
    Serial.println("Cartela cu ID-ul 3F F8 8C 29 \n");
  }
    if( x == 2 ) //addr == 2
  {
    Serial.println("\nCamera 3 ");
    Serial.println("Cartela cu ID-ul 6B BC 4A 73 \n");
  }
  cnt++;
}
//-----------------------------------------------------------------------------------------
void loop() {
  //---------------------------------------------------------------------------------------
  Wire.requestFrom(2, 8); //cerem 8 biti de la partea de intrare
  while (Wire.available()) { 
    //      
  }
  Wire.requestFrom(3, 8); //cerem 8 biti de la aprtea de iesire
  //---------------------------------------------------------------------------------------
  if (Serial.available() > 0) { // verifica daca avem date pe seriala
    state = mySerial.read(); // citeste datele de pe seriala
  }
  //---------------------------------------------------------------------------------------
  //verificam starea butonului
  stare_buton = digitalRead(BTN);
  if( stare_buton == HIGH )
  {
    w_state = '0';
  }
  else
  {
   //nimic 
   }  
  //---------------------------------------------------------------------------------------
  //partea de i2c
  Wire.requestFrom(2,1);//cerem date de la intrare
  //cerem de la poarta de intrare codul cordului RFID folosit
  while (Wire.available())
  {
    w_state = Wire.read();
    if ( w_state == '0' ) //daca butonul a fost apasat
    {
      //Serial.print("Intrare cu buton"); //testare
    }
    else{
      //
    }
        w_state = Wire.read();
    if ( w_state == '1' ) //daca poarta s-a deschis cu tag RFID
    {
      //Serial.print("Intrare cu card"); //testare
    }
    else{
      //
    }
  }
  //---------------------------------------------------------------------------------------
  //cand clientul pleaca ar trebuii anuntat la receptie
  //asta se paote implementa cu un buton cel mai simplu
  if( stare_buton == HIGH ){
      Wire.beginTransmission(3);//transmitem la iesire
      Wire.write('0'); //daca un client se decazeaza se va elibera camera
      Wire.endTransmission();
  }else
  {
      Wire.beginTransmission(3);//transmitem la iesire
      Wire.write('1'); // daca un client doreste sa paraseasca parcarea
      //dar nu s-a decazat nu se va elibera camera
      Wire.endTransmission();
  }
  //---------------------------------------------------------------------------------------
  Wire.requestFrom(3,1);//cerem date de la iesire
  while (Wire.available())
  {
    w_state = Wire.read();
    if ( w_state == '0' )
    {
      //s-a decazat camera 1
      EEPROM.write(0,CAM_GOL );
    }
    if ( w_state == '1' )
    {
      //s-a decazat camera 2
      EEPROM.write(1,CAM_GOL );
    }
    if ( w_state == '2' )
    {
      //s-a decazat camera 3
      EEPROM.write(2,CAM_GOL);
    }
  }
  //---------------------------------------------------------------------------------------
  //Serial.print(state);
  //pentru testare vom verifica datele
  //---------------------------------------------------------------------------------------
  //aici vom scrie date pe seriala
  //---------------------------------------------------------------------------------------
  //partea de scriere/citire EEPROM
  if( stare_buton == HIGH ) //daca avem client nou
  {
      //aici ar trebuii sa generez o factura si sa alog un card RFID
      if( addr == 0 ){
        EEPROM.write(addr,CAM1 );
        generare_factura(addr); //fucntia pentru generat factura
        EEPROM_increment();
      }
      if( addr == 1 ){
        EEPROM.write(addr,CAM2 );
        generare_factura(addr); //fucntia pentru generat factura
        EEPROM_increment();
      }
      if( addr == 2 ){
        EEPROM.write(addr,CAM3 );
        generare_factura(addr); //fucntia pentru generat factura
        //EEPROM_increment();
        //nu cred ca este necesar
      }
  }


  delay(100);
}
//-----------------------------------------------------------------------------------------
